import nu.studer.gradle.jooq.JooqGenerate
import org.flywaydb.gradle.task.AbstractFlywayTask
import org.flywaydb.gradle.task.FlywayCleanTask
import org.flywaydb.gradle.task.FlywayInfoTask
import org.flywaydb.gradle.task.FlywayMigrateTask

plugins {
    id("nu.studer.jooq") version "5.2"
    id("org.flywaydb.flyway") version "7.2.0"
}

dependencies {
    implementation("org.jooq:jooq:3.14.4")
    implementation("org.postgresql:postgresql:42.2.18")
    jooqGenerator("org.postgresql:postgresql:42.2.18")
}

fun setupFlywayDB(task: AbstractFlywayTask) {
    task.apply {
        url = "jdbc:${project.ext["DB_DRIVER"]}://${project.ext["DB_HOST"]}:${project.ext["DB_PORT"]}/${project.ext["DB_NAME"]}"
        user = project.ext["DB_USER"].toString()
        password = project.ext["DB_PASSWORD"].toString()
        schemas = arrayOf("core")
        locations = arrayOf(
            "filesystem:${project.projectDir}/resources/db/migrations/structure"
        )
    }
}

fun setupFlywayTestDB(task: AbstractFlywayTask) {
    task.apply {
        url = "jdbc:${project.ext["TEST_DB_DRIVER"]}://${project.ext["TEST_DB_HOST"]}:${project.ext["TEST_DB_PORT"]}/${project.ext["TEST_DB_NAME"]}"
        user = project.ext["TEST_DB_USER"].toString()
        password = project.ext["TEST_DB_PASSWORD"].toString()
        schemas = arrayOf("core")
        locations = arrayOf(
            "filesystem:${project.projectDir}/resources/db/migrations/structure"
        )
    }
}

tasks.register<FlywayMigrateTask>("flywayMigrateTest") { setupFlywayTestDB(this) }
tasks.register<FlywayCleanTask>("flywayCleanTest") { setupFlywayTestDB(this) }
tasks.register<FlywayInfoTask>("flywayInfoTest") { setupFlywayTestDB(this) }

tasks.getByName<AbstractFlywayTask>("flywayBaseline") { setupFlywayDB(this) }
tasks.getByName<AbstractFlywayTask>("flywayClean") { setupFlywayDB(this) }
tasks.getByName<AbstractFlywayTask>("flywayInfo") { setupFlywayDB(this) }
tasks.getByName<AbstractFlywayTask>("flywayMigrate") { setupFlywayDB(this) }
tasks.getByName<AbstractFlywayTask>("flywayRepair") { setupFlywayDB(this) }
tasks.getByName<AbstractFlywayTask>("flywayUndo") { setupFlywayDB(this) }
tasks.getByName<AbstractFlywayTask>("flywayValidate") { setupFlywayDB(this) }


jooq {
    version.set("3.14.4")

    configurations {
        create("main") {
            generateSchemaSourceOnCompilation.set(project.ext["GENERATE_JOOQ"] as Boolean)
            jooqConfiguration.apply {
                logging = org.jooq.meta.jaxb.Logging.DEBUG
                jdbc.apply {
                    driver = "org.postgresql.Driver"
                    url = "jdbc:${project.ext["DB_DRIVER"]}://${project.ext["DB_HOST"]}:${project.ext["DB_PORT"]}/${project.ext["DB_NAME"]}"
                    user = project.ext["DB_USER"].toString()
                    password = project.ext["DB_PASSWORD"].toString()
                }
                generator.apply {
                    name = "org.jooq.codegen.DefaultGenerator"
                    strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
                    database.apply {
                        name = "org.jooq.meta.postgres.PostgresDatabase"
                        inputSchema = "core"
                        includes = ".*"
                        excludes = ""
                    }
                    generate.apply {
                        isDeprecated = false
                        isRecords = true
                        isImmutablePojos = true
                        isFluentSetters = true
                    }
                    target.apply {
                        packageName = "generated.ldamore.zombies.core.infrastructure.persistence.jdbc.jooq"
                        directory = "generated"
                    }
                }
            }
        }
    }
}

tasks.named<JooqGenerate>("generateJooq") {
    dependsOn("flywayMigrate", "flywayMigrateTest")
inputs.files(fileTree("resources/db/migrations/structure"))
        .withPropertyName("migrations")
        .withPathSensitivity(PathSensitivity.RELATIVE)
    allInputsDeclared.set(true) // make jOOQ task participate in incremental builds and build caching
    outputs.cacheIf { true }
}
