package e2e

import ldamore.zombies.core.Core
import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivorCommand
import ldamore.zombies.core.application.commands.startGame.StartGameCommand

class FixtureCreator(private val core: Core) {
    val scenarios = Scenarios()

    inner class Scenarios {
        fun aStartedGameWithSurvivors() {
            core.command(AddSurvivorCommand("lautaro"))
            core.command(StartGameCommand())
        }
    }
}
