package infraestructure.jooqRepositories

import ldamore.zombies.core.infraestructure.persistance.jooq.jooqContextProvider
import ldamore.zombies.core.infraestructure.persistance.transactions.Transaction
import ldamore.zombies.core.infraestructure.persistance.transactions.TransactionManager
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag

@Tag("slow")
abstract class BaseRepositoryTest {
    private lateinit var  transactionManager: TransactionManager
    protected val provider = jooqContextProvider {
        credentialsFromEnv("TEST_DB")
        transactionManager = transactionManager()
    }
    private lateinit var currentTransaction: Transaction

    @BeforeEach
    fun setup() {
        currentTransaction = transactionManager.beginTransaction()
    }

    @AfterEach
    fun tearDown() {
        currentTransaction.rollback()
    }
}