package infraestructure.jooqRepositories

import ldamore.zombies.core.domain.zombie.Zombie
import ldamore.zombies.core.domain.zombie.Zombies
import ldamore.zombies.core.infraestructure.repositories.jooq.JooqRepositoryFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class JooqZombiesTest: BaseRepositoryTest() {
    @Test
    fun getByName() {
        val zombie = addZombie("lautaro")

        val zombiePersisted = zombies.get(zombie.name)!!

        assertAreEquals(zombiePersisted, zombie)
    }

    private fun assertAreEquals(zombiePersisted: Zombie, zombie: Zombie) {
        assertThat(zombiePersisted.id).isEqualTo(zombie.id)
        assertThat(zombiePersisted.name).isEqualTo(zombie.name)
        assertThat(zombiePersisted.wounds).isEqualTo(zombie.wounds)
    }

    private fun addZombie(name: String): Zombie {
        val id = zombies.nextId()
        val zombie = Zombie(id, name)
        zombies.add(zombie)
        return zombie
    }

    private val zombies = JooqRepositoryFactory(provider).cached().create<Zombies>()
}
