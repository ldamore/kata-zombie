package infraestructure.jooqRepositories

import ldamore.zombies.core.domain.survivor.Survivor
import ldamore.zombies.core.domain.survivor.Survivors
import ldamore.zombies.core.infraestructure.repositories.jooq.JooqRepositoryFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class JooqSurvivorsTest: BaseRepositoryTest() {
    @Test
    fun getAll() {
        val survivor1 = addSurvivor("lautaro")
        val survivor2 = addSurvivor("lucas")

        val survivors = survivors.getAll()

        assertThat(survivors[0].id).isEqualTo(survivor1.id)
        assertThat(survivors[1].id).isEqualTo(survivor2.id)
    }

    @Test
    fun getById() {
        val survivor = addSurvivor("lautaro")

        val survivorPersisted = survivors.get(survivor.id)

        assertAreEquals(survivorPersisted, survivor)
    }

    @Test
    fun getByName() {
        val survivor = addSurvivor("lautaro")

        val survivorPersisted = survivors.get(survivor.name)!!

        assertAreEquals(survivorPersisted, survivor)
    }

    private fun assertAreEquals(survivorPersisted: Survivor, survivor: Survivor) {
        assertThat(survivorPersisted.id).isEqualTo(survivor.id)
        assertThat(survivorPersisted.name).isEqualTo(survivor.name)
        assertThat(survivorPersisted.wounds).isEqualTo(survivor.wounds)
    }

    private fun addSurvivor(name: String): Survivor {
        val id = survivors.nextId()
        val survivor = Survivor(id, name)
        survivors.add(survivor)
        return survivor
    }

    private val survivors = JooqRepositoryFactory(provider).cached().create<Survivors>()
}
