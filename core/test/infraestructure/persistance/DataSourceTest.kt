package infraestructure.persistance

import ldamore.zombies.core.infraestructure.persistance.jdbc.DataSource
import ldamore.zombies.core.infraestructure.persistance.transactions.JdbcTransactionManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class DataSourceTest {
    @Test
    fun `get connection returns an active connection`() {
        val dataSource = DataSource(StubbedConnection(), JdbcTransactionManager())

        val connection = dataSource.getConnection()

        assertThat(connection.isClosed).isFalse
    }

    @Test
    fun `close connection close the specified connection`() {
        val dataSource = DataSource(StubbedConnection(), JdbcTransactionManager())
        val connection = dataSource.getConnection()

        dataSource.close(connection)

        assertThat(connection.isClosed).isTrue
    }

    @Test
    fun `getConnection creates a new connection on each call when there is not an active transaction`() {
        val dataSource = DataSource(StubbedConnection(), JdbcTransactionManager())

        val connection1 = dataSource.getConnection()
        val connection2 = dataSource.getConnection()

        assertThat(connection1).isNotEqualTo(connection2)
    }

    @Test
    fun `getConnection returns the active transaction's connection on each call when there is an active transaction`() {
        val transactionManager = JdbcTransactionManager()
        val dataSource = DataSource(StubbedConnection(), transactionManager)
        transactionManager.beginTransaction()

        val connection1 = dataSource.getConnection()
        val connection2 = dataSource.getConnection()

        assertThat(connection1).isEqualTo(connection2)
    }

    @Test
    fun `close should not close the connection of the active transaction`() {
        val transactionManager = JdbcTransactionManager()
        val dataSource = DataSource(StubbedConnection(), transactionManager)
        transactionManager.beginTransaction()
        val connection = transactionManager.activeConnection

        dataSource.close(connection!!)

        assertThat(connection.isClosed).isFalse
    }

    @Test
    fun `close should close a connection distinct from the active transaction's one`() {
        val transactionManager = JdbcTransactionManager()
        val dataSource = DataSource(StubbedConnection(), transactionManager)
        val connection = dataSource.getConnection()
        transactionManager.beginTransaction()

        dataSource.close(connection)

        assertThat(connection.isClosed).isTrue
    }
}
