package infraestructure.persistance

import ldamore.zombies.core.infraestructure.persistance.jdbc.DataSource
import ldamore.zombies.core.infraestructure.persistance.transactions.JdbcTransactionManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class JdbcTransactionManagerTest {
    @Test
    fun `there's no active connection if there's no active transaction`() {
        val dataSource = DataSource(StubbedConnection(), JdbcTransactionManager())
        val transactionManager = dataSource.transactionManager

        assertThat(transactionManager.activeConnection).isNull()
    }

    @Test
    fun `there's an active connection if there's an active transaction`() {
        val dataSource = DataSource(StubbedConnection(), JdbcTransactionManager())
        val transactionManager = dataSource.transactionManager

        transactionManager.beginTransaction()

        assertThat(transactionManager.activeConnection).isNotNull
    }

    @Test
    fun `when a transaction is rollbacked the connection is closed`() {
        val dataSource = DataSource(StubbedConnection(), JdbcTransactionManager())
        val transactionManager = dataSource.transactionManager
        val transaction = transactionManager.beginTransaction()
        val connection = transactionManager.activeConnection

        transaction.rollback()

        assertThat(transactionManager.activeConnection).isNull()
        assert(connection!!.isClosed)
    }

    @Test
    fun `when a transaction is rollbacked autocommit is enabled`() {
        val dataSource = DataSource(StubbedConnection(), JdbcTransactionManager())
        val transactionManager = dataSource.transactionManager
        val transaction = transactionManager.beginTransaction()
        val connection = transactionManager.activeConnection

        transaction.rollback()

        assertThat(connection?.autoCommit).isTrue
    }

}
