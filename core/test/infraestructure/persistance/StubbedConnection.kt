package infraestructure.persistance

import ldamore.zombies.core.infraestructure.persistance.jdbc.JdbcConnection

class StubbedConnection(private val savePoints: SavePoints = SavePoints()): JdbcConnection {
    override fun getConnection() = ConnectionStub(savePoints)
}
