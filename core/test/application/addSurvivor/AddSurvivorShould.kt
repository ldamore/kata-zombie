package application.addSurvivor

import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivor
import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivorCommand
import ldamore.zombies.core.domain.survivor.SurvivorId
import ldamore.zombies.core.domain.survivor.SurvivorNameAlreadyExist
import ldamore.zombies.core.domain.survivor.Survivors
import ldamore.zombies.core.infraestructure.repositories.inMemory.InMemoryRepositoryFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class AddSurvivorShould {
    @Test
    fun `create a survivor with the specified name`() {
        val survivorName = "lautaro"

        addSurvivor.execute(AddSurvivorCommand(survivorName))

        with(getSurvivor(lautaroSurvivorId)) {
            assertThat(this.name).isEqualTo(survivorName)
        }
    }

    @Test
    fun `created survivor starts with 0 wounds`() {
        val survivorName = "lautaro"

        addSurvivor.execute(AddSurvivorCommand(survivorName))

        with(getSurvivor(lautaroSurvivorId)) {
            assertThat(this.wounds).isEqualTo(0)
        }
    }

    @Test
    fun `fail if survivor name already exist`() {
        val survivorName = "lautaro"
        addSurvivor.execute(AddSurvivorCommand(survivorName))

        assertThrows<SurvivorNameAlreadyExist> {
            addSurvivor.execute(AddSurvivorCommand(survivorName))
        }
    }

    private fun getSurvivor(id: SurvivorId) = repositories.create<Survivors>().get(id)

    private val lautaroSurvivorId = SurvivorId(1)
    private val repositories = InMemoryRepositoryFactory().cached()
    private val addSurvivor = AddSurvivor(repositories.create())
}