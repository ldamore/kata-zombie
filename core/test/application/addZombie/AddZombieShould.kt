package application.addZombie

import ldamore.zombies.core.application.commands.addZombie.AddZombie
import ldamore.zombies.core.application.commands.addZombie.AddZombieCommand
import ldamore.zombies.core.domain.zombie.ZombieNameAlreadyExist
import ldamore.zombies.core.domain.zombie.Zombies
import ldamore.zombies.core.infraestructure.repositories.inMemory.InMemoryRepositoryFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class AddZombieShould {
    @Test
    fun `create a zombie with the specified name`() {
        val zombieName = "paul"

        addZombie.execute(AddZombieCommand(zombieName))

        with(getZombie(zombieName)!!) {
            assertThat(this.name).isEqualTo(zombieName)
        }
    }

    @Test
    fun `created zombie starts with 0 wounds`() {
        val zombieName = "paul"

        addZombie.execute(AddZombieCommand(zombieName))

        with(getZombie(zombieName)!!) {
            assertThat(this.wounds).isEqualTo(0)
        }
    }

    @Test
    fun `fail if zombie name already exist`() {
        val zombieName = "paul"
        addZombie.execute(AddZombieCommand(zombieName))

        assertThrows<ZombieNameAlreadyExist> {
            addZombie.execute(AddZombieCommand(zombieName))
        }
    }

    private fun getZombie(name: String) = repositories.create<Zombies>().get(name)

    private val repositories = InMemoryRepositoryFactory().cached()
    private val addZombie = AddZombie(repositories.create())
}