package application

import ldamore.zombies.core.application.ApplicationServiceNoRegistered
import ldamore.zombies.core.application.Mediator
import ldamore.zombies.core.application.commands.Command
import ldamore.zombies.core.application.commands.CommandHandler
import ldamore.zombies.core.application.queries.Query
import ldamore.zombies.core.application.queries.QueryHandler
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class MediatorShould {
    @Test
    fun `execute the given command`() {
        val myCommand = MyCommand()
        val myCommandHandler = MyCommandHandler()
        mediator.register(MyCommand::class, myCommandHandler)

        mediator.execute(myCommand)

        assertThat(myCommandHandler.executedCommand).isEqualTo(myCommand)
    }

    @Test
    fun `execute the given query`() {
        val myQuery = MyQuery()
        val myQueryHandler = MyQueryHandler()
        mediator.register(MyQuery::class, myQueryHandler)

        val response = mediator.execute(myQuery)

        assertThat(response.fruits).isEqualTo(listOf("apple", "banana"))
    }

    @Test
    fun `fail if any command handlers aren't registered for the specified command`() {
        val mediator = Mediator()
        val myCommand = MyCommand()

        assertThrows<ApplicationServiceNoRegistered> { mediator.execute(myCommand) }
    }

    @Test
    fun `fail if any query handlers aren't registered for the specified query`() {
        val mediator = Mediator()
        val myQuery = MyQuery()

        assertThrows<ApplicationServiceNoRegistered> { mediator.execute(myQuery) }
    }

    private val mediator = Mediator()

    class MyCommand: Command

    class MyCommandHandler: CommandHandler<MyCommand> {
        var executedCommand: Command? = null

        override fun execute(command: MyCommand) {
            executedCommand = command
        }
    }

    class MyQuery: Query<MyQuery.Response> {
        data class Response(val fruits: List<String>)
    }

    class MyQueryHandler(private val fruits: List<String> = listOf("apple", "banana")): QueryHandler<MyQuery, MyQuery.Response> {
        override fun execute(query: MyQuery) = MyQuery.Response(fruits)
    }
}
