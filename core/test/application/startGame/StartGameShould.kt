package application.startGame

import ldamore.zombies.core.application.commands.startGame.StartGame
import ldamore.zombies.core.application.commands.startGame.StartGameCommand
import ldamore.zombies.core.domain.game.CurrentGame
import ldamore.zombies.core.domain.survivor.Survivor
import ldamore.zombies.core.domain.survivor.SurvivorId
import ldamore.zombies.core.domain.survivor.Survivors
import ldamore.zombies.core.domain.zombie.Zombie
import ldamore.zombies.core.domain.zombie.ZombieId
import ldamore.zombies.core.domain.zombie.Zombies
import ldamore.zombies.core.infraestructure.repositories.inMemory.InMemoryRepositoryFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class StartGameShould {
    @Test
    fun `start the game if has any survivor and zombies`() {
        addSurvivorsAndZombies()

        startGame.execute(StartGameCommand())

        val game = currentGame.getCurrent()
        assertThat(game!!.isActive).isTrue
    }

    private fun addSurvivorsAndZombies() {
        repositories.create<Survivors>().add(Survivor(SurvivorId(1), "lautaro"))
        repositories.create<Zombies>().add(Zombie(ZombieId(1), "paul"))
    }

    private val currentGame = CurrentGame()
    private val startGame = StartGame(currentGame)
    private val repositories = InMemoryRepositoryFactory()
}