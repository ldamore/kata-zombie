package ldamore.zombies.core

import ldamore.zombies.core.application.Mediator
import ldamore.zombies.core.domain.game.CurrentGame
import ldamore.zombies.core.infraestructure.repositories.RepositoryFactory

class CoreConfiguration(
    val mediator: Mediator = Mediator(),
    val repositoryFactory: RepositoryFactory,
    val currentGame: CurrentGame,
)
