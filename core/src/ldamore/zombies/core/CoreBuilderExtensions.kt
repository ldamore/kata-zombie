package ldamore.zombies.core

import ldamore.zombies.core.infraestructure.persistance.jooq.JooqContextProviderBuilder
import ldamore.zombies.core.infraestructure.repositories.RepositoryFactory
import ldamore.zombies.core.infraestructure.repositories.jooq.JooqRepositoryFactory


fun CoreBuilder.jooqRepositories(cached: Boolean = false, addDetails: JooqContextProviderBuilder.() -> Unit): RepositoryFactory {
    val jooqContextProvider = JooqContextProviderBuilder().also(addDetails).build()
    return this.repositoryFactory(JooqRepositoryFactory(jooqContextProvider), cached)
}
