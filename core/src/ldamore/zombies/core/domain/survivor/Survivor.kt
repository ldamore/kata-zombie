package ldamore.zombies.core.domain.survivor

class Survivor(val id: SurvivorId, val name: String) {
    var wounds = 0

    override fun equals(other: Any?) = other is Survivor && other.id == id
    override fun hashCode() = id.hashCode()

    companion object {
        fun from(snapshot: Snapshot): Survivor {
            return Survivor(snapshot.id, snapshot.name).apply {
                wounds = snapshot.wounds
            }
        }
    }

    data class Snapshot(val id: SurvivorId, val name: String, val wounds: Int)
}