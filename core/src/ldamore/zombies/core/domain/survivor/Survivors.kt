package ldamore.zombies.core.domain.survivor

interface Survivors {
    fun nextId(): SurvivorId
    fun add(survivor: Survivor)
    fun getAll(): List<Survivor>
    fun get(id: SurvivorId): Survivor
    fun get(name: String): Survivor?
}
