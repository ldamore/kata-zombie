package ldamore.zombies.core.domain.survivor

import ldamore.zombies.core.domain.DomainError

class SurvivorNameAlreadyExist(name: String): DomainError("Survivor with name: $name already exists")
