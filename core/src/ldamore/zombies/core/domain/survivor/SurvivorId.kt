package ldamore.zombies.core.domain.survivor

class SurvivorId(private val rawId: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SurvivorId

        if (rawId != other.rawId) return false

        return true
    }

    override fun hashCode() = rawId

    fun toInt() = rawId
}
