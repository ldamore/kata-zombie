package ldamore.zombies.core.domain.zombie

import ldamore.zombies.core.domain.DomainError

class ZombieNameAlreadyExist(name: String): DomainError("Zombie with name: $name already exists")
