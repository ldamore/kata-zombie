package ldamore.zombies.core.domain.zombie

interface Zombies {
    fun get(name: String): Zombie?
    fun nextId(): ZombieId
    fun add(zombie: Zombie)
}