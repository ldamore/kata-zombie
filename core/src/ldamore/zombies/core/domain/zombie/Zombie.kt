package ldamore.zombies.core.domain.zombie

class Zombie(val id: ZombieId, val name: String) {
    var wounds = 0

    override fun equals(other: Any?) = other is Zombie && other.id == id
    override fun hashCode() = id.hashCode()

    companion object {
        fun from(snapshot: Snapshot): Zombie {
            return Zombie(snapshot.id, snapshot.name).apply {
                wounds = snapshot.wounds
            }
        }
    }

    data class Snapshot(val id: ZombieId, val name: String, val wounds: Int)
}