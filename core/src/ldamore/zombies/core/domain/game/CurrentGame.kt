package ldamore.zombies.core.domain.game

class CurrentGame {
    private var game: Game? = null

    fun setCurrent(game: Game) {
        this.game = game
    }

    fun getCurrent() = game
}
