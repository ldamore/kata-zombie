package ldamore.zombies.core.domain.game

import ldamore.zombies.core.domain.DomainError

class GameNotStartedError: DomainError("Game is not started")
