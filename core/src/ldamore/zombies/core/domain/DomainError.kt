package ldamore.zombies.core.domain

abstract class DomainError(message: String): Throwable(message)