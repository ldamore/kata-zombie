package ldamore.zombies.core.infraestructure.persistance.transactions

import ldamore.zombies.core.infraestructure.persistance.jdbc.DataSource
import java.sql.Connection

class JdbcTransactionManager(): TransactionManager {
    private var dataSource: DataSource? = null
    private var activeTransaction: JdbcTransaction? = null

    val activeConnection: Connection?
        get() = activeTransaction?.connection

    fun connectToDataSource(dataSource: DataSource) { this.dataSource = dataSource }

    override fun beginTransaction(): Transaction {
        activeTransaction = JdbcTransaction(dataSource!!.getConnection(), ::onClose)
        return activeTransaction!!
    }

    private fun onClose() {
        val connection = activeConnection!!
        activeTransaction = null
        dataSource?.close(connection)
    }

    fun hasActiveTransaction() = activeTransaction != null
}
