package ldamore.zombies.core.infraestructure.persistance.transactions

interface Transaction {
    val isClosed: Boolean
    fun rollback()
}
