package ldamore.zombies.core.infraestructure.persistance.transactions

interface TransactionManager {
    fun beginTransaction(): Transaction
}
