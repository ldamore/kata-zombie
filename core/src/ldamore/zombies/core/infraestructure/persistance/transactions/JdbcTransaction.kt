package ldamore.zombies.core.infraestructure.persistance.transactions

import java.sql.Connection

class JdbcTransaction(val connection: Connection, val onClose: () -> Unit): Transaction {
    override var isClosed = false
        private set

    init {
        connection.autoCommit = false
    }

    override fun rollback() {
        if (isClosed) return

        connection.rollback()
        connection.autoCommit = true
        closeConnection()
    }

    private fun closeConnection() {
        onClose()
        isClosed = true
    }
}