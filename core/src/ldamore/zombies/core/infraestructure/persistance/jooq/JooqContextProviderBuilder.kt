package ldamore.zombies.core.infraestructure.persistance.jooq

import ldamore.zombies.core.infraestructure.Environment
import ldamore.zombies.core.infraestructure.persistance.jdbc.*
import ldamore.zombies.core.infraestructure.persistance.transactions.JdbcTransactionManager

class JooqContextProviderBuilder {
    private var credentials = JdbcCredentials("", "", JdbcUrl("", "", 0, ""))
    private var createConnectionFactory: () -> JdbcConnection = { SimpleJdbcConnection(credentials) }
    private var transactionManager: JdbcTransactionManager = JdbcTransactionManager()

    fun credentialsFromEnv(prefix: String = "DB"): JdbcCredentials {
        return JdbcCredentialsFromEnvironmentFactory(Environment).create(prefix).also { this.credentials = it }
    }

    fun simpleConnections() { createConnectionFactory = { SimpleJdbcConnection(credentials) } }

    fun transactionManager() = this.transactionManager

    fun build(): JooqProvider {
        val dataSource = DataSource(createConnectionFactory(), transactionManager)
        return SimpleJooqProvider(dataSource)
    }
}

fun jooqContextProvider(addDetails: JooqContextProviderBuilder.() -> Unit): JooqProvider {
    return JooqContextProviderBuilder().also(addDetails).build()
}
