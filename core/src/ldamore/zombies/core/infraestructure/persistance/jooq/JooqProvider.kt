package ldamore.zombies.core.infraestructure.persistance.jooq

import org.jooq.DSLContext

interface JooqProvider {
    fun context(): DSLContext
}