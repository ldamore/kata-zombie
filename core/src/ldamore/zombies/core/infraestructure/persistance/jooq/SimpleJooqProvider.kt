package ldamore.zombies.core.infraestructure.persistance.jooq

import ldamore.zombies.core.infraestructure.persistance.jdbc.DataSource
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL

class SimpleJooqProvider(private val dataSource: DataSource): JooqProvider {
    override fun context(): DSLContext {
        System.getProperties().setProperty("org.jooq.no-logo", "true")
        return DSL.using(dataSource.getConnection(), SQLDialect.POSTGRES)
    }
}