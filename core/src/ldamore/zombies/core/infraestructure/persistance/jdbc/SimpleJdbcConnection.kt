package ldamore.zombies.core.infraestructure.persistance.jdbc

import java.sql.Connection
import java.sql.DriverManager

class SimpleJdbcConnection(private val jdbcCredentials: JdbcCredentials): JdbcConnection {
    override fun getConnection(): Connection {
        return DriverManager.getConnection(
            jdbcCredentials.jdbcUrl.toString(),
            jdbcCredentials.username,
            jdbcCredentials.password
        )
    }
}
