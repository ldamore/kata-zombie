package ldamore.zombies.core.infraestructure.persistance.jdbc

import ldamore.zombies.core.infraestructure.Environment

class JdbcCredentialsFromEnvironmentFactory(private val environment: Environment) {
    fun create(prefix: String): JdbcCredentials {
        return JdbcCredentials(
            jdbcUrl = JdbcUrl(
                environment["${prefix}_DRIVER"],
                environment["${prefix}_HOST"],
                environment["${prefix}_PORT"].toInt(),
                environment["${prefix}_NAME"]
            ),
            username = environment["${prefix}_USER"],
            password = environment["${prefix}_PASSWORD"]
        )
    }
}
