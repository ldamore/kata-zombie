package ldamore.zombies.core.infraestructure.persistance.jdbc

import ldamore.zombies.core.infraestructure.persistance.transactions.JdbcTransactionManager
import java.sql.Connection

class DataSource(private val jdbcConnection: JdbcConnection, val transactionManager: JdbcTransactionManager) {
    init {
        transactionManager.connectToDataSource(this)
    }

    fun getConnection(): Connection {
        if (transactionManager.hasActiveTransaction()) return transactionManager.activeConnection!!
        return jdbcConnection.getConnection()
    }

    fun close(connection: Connection) {
        if (isTheTransactionManagerConnection(connection)) return
        connection.close()
    }

    private fun isTheTransactionManagerConnection(connection: Connection) =
        transactionManager.activeConnection == connection
}
