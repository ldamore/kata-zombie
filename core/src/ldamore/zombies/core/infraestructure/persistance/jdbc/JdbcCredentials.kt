package ldamore.zombies.core.infraestructure.persistance.jdbc

class JdbcCredentials(val username: String, val password: String, val jdbcUrl: JdbcUrl) {
    override fun toString(): String {
        return "JdbcCredentials(username='$username', password='$password', jdbcUrl=$jdbcUrl)"
    }
}
