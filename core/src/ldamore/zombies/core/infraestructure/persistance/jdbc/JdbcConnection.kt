package ldamore.zombies.core.infraestructure.persistance.jdbc

import java.sql.Connection

interface JdbcConnection {
    fun getConnection(): Connection
}