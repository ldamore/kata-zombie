package ldamore.zombies.core.infraestructure.persistance.jdbc

class JdbcUrl(val driver: String, val host: String, val port: Int, val database: String) {
    override fun toString(): String {
        return "jdbc:$driver://$host:$port/$database"
    }

}