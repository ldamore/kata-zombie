package ldamore.zombies.core.infraestructure.repositories

import kotlin.reflect.KClass

abstract class RepositoryFactory {
    abstract fun <T: Any> create(repositoryType: KClass<T>): T

    inline fun <reified T: Any> create() = create(T::class)

    fun cached() = CachedRepositoryFactory(this)
}
