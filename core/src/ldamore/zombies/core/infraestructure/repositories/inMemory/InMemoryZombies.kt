package ldamore.zombies.core.infraestructure.repositories.inMemory

import ldamore.zombies.core.domain.zombie.Zombie
import ldamore.zombies.core.domain.zombie.ZombieId
import ldamore.zombies.core.domain.zombie.ZombieNameAlreadyExist
import ldamore.zombies.core.domain.zombie.Zombies

class InMemoryZombies: Zombies {
    private val zombies = mutableListOf<Zombie>()
    private var id = 1

    override fun nextId() = ZombieId(id++)

    override fun add(zombie: Zombie) {
        if (zombies.any { it.name == zombie.name }) throw ZombieNameAlreadyExist(zombie.name)
        zombies.add(zombie)
    }

    override fun get(name: String) = zombies.singleOrNull { it.name == name }
}
