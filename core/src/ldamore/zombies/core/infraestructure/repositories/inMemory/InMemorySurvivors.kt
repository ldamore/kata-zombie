package ldamore.zombies.core.infraestructure.repositories.inMemory

import ldamore.zombies.core.domain.survivor.Survivor
import ldamore.zombies.core.domain.survivor.SurvivorId
import ldamore.zombies.core.domain.survivor.SurvivorNameAlreadyExist
import ldamore.zombies.core.domain.survivor.Survivors

class InMemorySurvivors: Survivors {
    private val survivors = mutableListOf<Survivor>()
    private var id = 1

    override fun nextId() = SurvivorId(id++)

    override fun add(survivor: Survivor) {
        if (survivors.any { it.name == survivor.name }) throw SurvivorNameAlreadyExist(survivor.name)
        survivors.add(survivor)
    }

    override fun getAll(): List<Survivor> {
        return survivors.toList()
    }

    override fun get(id: SurvivorId) = survivors.single { it.id == id }

    override fun get(name: String) = survivors.singleOrNull { it.name == name }
}
