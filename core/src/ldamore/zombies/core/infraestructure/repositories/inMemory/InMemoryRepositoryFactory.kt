@file:Suppress("UNCHECKED_CAST", "IMPLICIT_CAST_TO_ANY")

package ldamore.zombies.core.infraestructure.repositories.inMemory

import ldamore.zombies.core.domain.survivor.Survivors
import ldamore.zombies.core.domain.zombie.Zombies
import ldamore.zombies.core.infraestructure.repositories.RepositoryFactory
import kotlin.reflect.KClass

class InMemoryRepositoryFactory: RepositoryFactory() {
    override fun <T: Any> create(repositoryType: KClass<T>): T {
        return when(repositoryType) {
            Survivors::class -> InMemorySurvivors()
            Zombies::class -> InMemoryZombies()
            else -> throw NotImplementedError("InMemory${repositoryType.simpleName} not implemented")
        } as T
    }
}
