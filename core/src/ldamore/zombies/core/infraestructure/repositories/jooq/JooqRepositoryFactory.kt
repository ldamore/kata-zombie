@file:Suppress("UNCHECKED_CAST", "IMPLICIT_CAST_TO_ANY")

package ldamore.zombies.core.infraestructure.repositories.jooq

import ldamore.zombies.core.domain.survivor.Survivors
import ldamore.zombies.core.domain.zombie.Zombies
import ldamore.zombies.core.infraestructure.persistance.jooq.JooqProvider
import ldamore.zombies.core.infraestructure.repositories.RepositoryFactory
import kotlin.reflect.KClass

class JooqRepositoryFactory(private val jooqProvider: JooqProvider): RepositoryFactory() {
    override fun <T: Any> create(repositoryType: KClass<T>): T {
        return when(repositoryType) {
            Survivors::class -> JooqSurvivors(jooqProvider)
            Zombies::class -> JooqZombies(jooqProvider)
            else -> throw NotImplementedError("Jooq${repositoryType.simpleName} not implemented")
        } as T
    }
}
