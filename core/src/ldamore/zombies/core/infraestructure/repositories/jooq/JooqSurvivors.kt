package ldamore.zombies.core.infraestructure.repositories.jooq

import generated.ldamore.zombies.core.infrastructure.persistence.jdbc.jooq.Sequences.SURVIVORS_ID_SEQ
import generated.ldamore.zombies.core.infrastructure.persistence.jdbc.jooq.Tables.SURVIVORS
import generated.ldamore.zombies.core.infrastructure.persistence.jdbc.jooq.tables.records.SurvivorsRecord
import ldamore.zombies.core.domain.survivor.Survivor
import ldamore.zombies.core.domain.survivor.Survivor.Snapshot
import ldamore.zombies.core.domain.survivor.SurvivorId
import ldamore.zombies.core.domain.survivor.SurvivorNameAlreadyExist
import ldamore.zombies.core.domain.survivor.Survivors
import ldamore.zombies.core.infraestructure.persistance.jooq.JooqProvider
import org.jooq.exception.DataAccessException

class JooqSurvivors(private val jooqProvider: JooqProvider): Survivors {
    override fun nextId(): SurvivorId {
        return SurvivorId(jooqProvider.context().nextval(SURVIVORS_ID_SEQ))
    }

    override fun add(survivor: Survivor) {
        try {
            val context = jooqProvider.context()
            context.insertInto(
                SURVIVORS,
                SURVIVORS.ID,
                SURVIVORS.NAME,
                SURVIVORS.WOUNDS,
            ).values(
                survivor.id.toInt(),
                survivor.name,
                survivor.wounds,
            ).execute()
        } catch (e: DataAccessException) {
            throw SurvivorNameAlreadyExist(survivor.name)
        }
    }

    override fun getAll(): List<Survivor> {
        val context = jooqProvider.context()
        return context
            .selectFrom(SURVIVORS)
            .orderBy(SURVIVORS.ID)
            .fetch()
            .map { survivorFromRecord(it) }
    }

    override fun get(id: SurvivorId): Survivor {
        val context = jooqProvider.context()
        return context
            .selectFrom(SURVIVORS)
            .where(SURVIVORS.ID.equal(id.toInt()))
            .fetch()
            .map { survivorFromRecord(it) }
            .single()
    }

    override fun get(name: String): Survivor? {
        val context = jooqProvider.context()
        return context
            .selectFrom(SURVIVORS)
            .where(SURVIVORS.NAME.equal(name))
            .fetch()
            .map { survivorFromRecord(it) }
            .singleOrNull()
    }

    private fun survivorFromRecord(record: SurvivorsRecord): Survivor {
        return with(record) {
            Survivor.from(Snapshot(SurvivorId(id), name, wounds))
        }
    }
}