package ldamore.zombies.core.infraestructure.repositories.jooq

import generated.ldamore.zombies.core.infrastructure.persistence.jdbc.jooq.Sequences
import generated.ldamore.zombies.core.infrastructure.persistence.jdbc.jooq.Tables
import generated.ldamore.zombies.core.infrastructure.persistence.jdbc.jooq.tables.records.ZombiesRecord
import ldamore.zombies.core.domain.zombie.Zombie
import ldamore.zombies.core.domain.zombie.Zombie.Snapshot
import ldamore.zombies.core.domain.zombie.ZombieId
import ldamore.zombies.core.domain.zombie.ZombieNameAlreadyExist
import ldamore.zombies.core.domain.zombie.Zombies
import ldamore.zombies.core.infraestructure.persistance.jooq.JooqProvider
import org.jooq.exception.DataAccessException

class JooqZombies(private val jooqProvider: JooqProvider): Zombies {
    override fun get(name: String): Zombie? {
        val context = jooqProvider.context()
        return context
            .selectFrom(Tables.ZOMBIES)
            .where(Tables.ZOMBIES.NAME.equal(name))
            .fetch()
            .map { zombieFromRecord(it) }
            .singleOrNull()
    }

    override fun nextId(): ZombieId {
        return ZombieId(jooqProvider.context().nextval(Sequences.ZOMBIES_ID_SEQ))
    }

    override fun add(zombie: Zombie) {
        try {
            val context = jooqProvider.context()
            context.insertInto(
                Tables.ZOMBIES,
                Tables.ZOMBIES.ID,
                Tables.ZOMBIES.NAME,
                Tables.ZOMBIES.WOUNDS,
            ).values(
                zombie.id.toInt(),
                zombie.name,
                zombie.wounds,
            ).execute()
        } catch (e: DataAccessException) {
            throw ZombieNameAlreadyExist(zombie.name)
        }
    }

    private fun zombieFromRecord(record: ZombiesRecord): Zombie {
        return with(record) {
            Zombie.from(Snapshot(ZombieId(id), name, wounds))
        }
    }
}