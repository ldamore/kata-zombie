@file:Suppress("UNCHECKED_CAST")

package ldamore.zombies.core.infraestructure.repositories

import kotlin.reflect.KClass

class CachedRepositoryFactory(private val factory: RepositoryFactory): RepositoryFactory() {
    private val cache = mutableMapOf<KClass<*>, Any>()

    override fun <T: Any> create(repositoryType: KClass<T>): T {
        return cache.getOrPut(repositoryType) { factory.create(repositoryType) } as T
    }
}
