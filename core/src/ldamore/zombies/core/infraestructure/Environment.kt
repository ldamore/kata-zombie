package ldamore.zombies.core.infraestructure

import io.github.cdimascio.dotenv.dotenv
import java.nio.file.Files
import java.nio.file.Paths

object Environment {
    private val dotenv = dotenv {
        directory = pathToEnvFile()
        ignoreIfMissing = true
    }

    operator fun get(name: String): String {
        return dotenv[name].toString()
    }

    private fun pathToEnvFile(): String {
        var path = Paths.get("./").toAbsolutePath().normalize()
        do {
            if (Files.exists(path.resolve(".env.dist"))) {
                return path.toString()
            }
            path = path.parent
        } while (path != null)
        return "./"
    }
}
