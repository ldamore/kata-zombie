package ldamore.zombies.core

import ldamore.zombies.core.application.commands.Command
import ldamore.zombies.core.application.queries.Query

class Core(private val config: CoreConfiguration) {
    init {
        CoreStartUp.startWith(config)
    }

    fun <T: Command> command(command: T) {
        config.mediator.execute(command)
    }

    fun <R, T: Query<R>> query(query: T): R {
        return config.mediator.execute(query)
    }
}
