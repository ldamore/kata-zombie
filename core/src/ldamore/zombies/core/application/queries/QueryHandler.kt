package ldamore.zombies.core.application.queries

interface QueryHandler<T: Query<TResult>, TResult> {
    fun execute(query: T): TResult
}