package ldamore.zombies.core.application.queries.getGame

import ldamore.zombies.core.application.queries.QueryHandler
import ldamore.zombies.core.application.queries.getGame.GetGameQuery.Response
import ldamore.zombies.core.domain.game.CurrentGame

class GetGame(private val currentGame: CurrentGame): QueryHandler<GetGameQuery, Response> {
    override fun execute(query: GetGameQuery): Response {
        val game = currentGame.getCurrent()
        return Response(listOf(), game?.isActive)
    }
}
