package ldamore.zombies.core.application.queries.getGame

import ldamore.zombies.core.application.queries.Query

class GetGameQuery: Query<GetGameQuery.Response> {
    data class Response(val survivors: List<SurvivorData>, val isActive: Boolean?)
    data class SurvivorData(val name: String)
}