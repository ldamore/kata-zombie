package ldamore.zombies.core.application.queries.getSurvivors

import ldamore.zombies.core.application.queries.Query

class GetSurvivorsQuery: Query<GetSurvivorsQuery.Response> {
    data class Response(val survivors: List<SurvivorData>) {
        data class SurvivorData(val id: Int, val name: String)
    }
}