package ldamore.zombies.core.application.queries.getSurvivors

import ldamore.zombies.core.application.queries.QueryHandler
import ldamore.zombies.core.application.queries.getSurvivors.GetSurvivorsQuery.Response
import ldamore.zombies.core.application.queries.getSurvivors.GetSurvivorsQuery.Response.SurvivorData
import ldamore.zombies.core.domain.survivor.Survivors

class GetSurvivors(private val survivors: Survivors): QueryHandler<GetSurvivorsQuery, Response> {
    override fun execute(query: GetSurvivorsQuery): Response {
        return Response(survivors.getAll().map { SurvivorData(it.id.toInt(), it.name) })
    }
}
