package ldamore.zombies.core.application

import ldamore.zombies.core.application.commands.Command
import ldamore.zombies.core.application.commands.CommandHandler
import ldamore.zombies.core.application.queries.Query
import ldamore.zombies.core.application.queries.QueryHandler
import kotlin.reflect.KClass

class Mediator {
    val commandHandlers = mutableMapOf<KClass<*>, CommandHandler<*>>()
    val queryHandlers = mutableMapOf<KClass<*>, QueryHandler<*, *>>()

    fun execute(command: Command) {
        val commandHandler = getOrFailIfNotRegistered(command)
        commandHandler.execute(command)
    }

    fun <TResult>execute(query: Query<TResult>): TResult {
        val queryHandler = getOrFailIfNotRegistered(query)
        return queryHandler.execute(query)
    }

    fun <T: Command>register(commandType: KClass<T>, commandHandler: CommandHandler<T>) {
        commandHandlers[commandType] = commandHandler
    }

    inline fun <reified T: Command> register(commandHandler: CommandHandler<T>) {
        commandHandlers[T::class] = commandHandler
    }

    inline fun <reified T: Query<TResult>, TResult> register(queryHandler: QueryHandler<T, TResult>) {
        queryHandlers[T::class] = queryHandler
    }

    fun <T: Query<TResult>, TResult>register(query: KClass<T>, queryHandler: QueryHandler<T, TResult>) {
        queryHandlers[query] = queryHandler
    }

    private fun getOrFailIfNotRegistered(command: Command): CommandHandler<Command> {
        val commandHandler = commandHandlers[command::class]
            ?: throw ApplicationServiceNoRegistered("Command handler for command: $command")
        return commandHandler as CommandHandler<Command>
    }

    private fun <TResult>getOrFailIfNotRegistered(query: Query<TResult>): QueryHandler<Query<TResult>, TResult> {
        val queryHandler = queryHandlers[query::class]
            ?: throw ApplicationServiceNoRegistered("Query handler for query: $query")
        return queryHandler as QueryHandler<Query<TResult>, TResult>
    }
}
