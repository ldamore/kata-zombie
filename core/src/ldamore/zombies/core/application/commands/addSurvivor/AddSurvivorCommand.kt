package ldamore.zombies.core.application.commands.addSurvivor

import ldamore.zombies.core.application.commands.Command

class AddSurvivorCommand(val survivorName: String): Command {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AddSurvivorCommand

        if (survivorName != other.survivorName) return false

        return true
    }

    override fun hashCode(): Int {
        return survivorName.hashCode()
    }

    override fun toString(): String {
        return "add-survivor"
    }
}
