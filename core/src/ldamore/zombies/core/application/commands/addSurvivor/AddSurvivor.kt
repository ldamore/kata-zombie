package ldamore.zombies.core.application.commands.addSurvivor

import ldamore.zombies.core.application.commands.CommandHandler
import ldamore.zombies.core.domain.survivor.Survivor
import ldamore.zombies.core.domain.survivor.Survivors

class AddSurvivor(private val survivors: Survivors): CommandHandler<AddSurvivorCommand> {
    override fun execute(command: AddSurvivorCommand) {
        val survivorId = survivors.nextId()
        val survivor = Survivor(survivorId, command.survivorName)
        survivors.add(survivor)
    }
}
