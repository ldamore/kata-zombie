package ldamore.zombies.core.application.commands

interface CommandHandler<T: Command> {
    fun execute(command: T)
}