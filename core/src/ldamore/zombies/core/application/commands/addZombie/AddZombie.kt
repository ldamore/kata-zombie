package ldamore.zombies.core.application.commands.addZombie

import ldamore.zombies.core.application.commands.CommandHandler
import ldamore.zombies.core.domain.zombie.Zombie
import ldamore.zombies.core.domain.zombie.Zombies

class AddZombie(private val zombies: Zombies): CommandHandler<AddZombieCommand> {
    override fun execute(command: AddZombieCommand) {
        val zombieId = zombies.nextId()
        val zombie = Zombie(zombieId, command.zombieName)
        zombies.add(zombie)
    }
}