package ldamore.zombies.core.application.commands.addZombie

import ldamore.zombies.core.application.commands.Command

class AddZombieCommand(val zombieName: String): Command {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AddZombieCommand

        if (zombieName != other.zombieName) return false

        return true
    }

    override fun hashCode(): Int {
        return zombieName.hashCode()
    }

    override fun toString(): String {
        return "add-zombie"
    }
}
