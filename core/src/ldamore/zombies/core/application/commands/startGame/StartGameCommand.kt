package ldamore.zombies.core.application.commands.startGame

import ldamore.zombies.core.application.commands.Command

class StartGameCommand: Command {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}