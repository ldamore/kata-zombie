package ldamore.zombies.core.application.commands.startGame

import ldamore.zombies.core.application.commands.CommandHandler
import ldamore.zombies.core.domain.game.CurrentGame
import ldamore.zombies.core.domain.game.Game

class StartGame(private val currentGame: CurrentGame): CommandHandler<StartGameCommand> {
    override fun execute(command: StartGameCommand) {
        val game = Game()
        currentGame.setCurrent(game)
    }
}
