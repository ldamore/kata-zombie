package ldamore.zombies.core

import ldamore.zombies.core.application.Mediator
import ldamore.zombies.core.domain.game.CurrentGame
import ldamore.zombies.core.infraestructure.repositories.RepositoryFactory
import ldamore.zombies.core.infraestructure.repositories.inMemory.InMemoryRepositoryFactory

class CoreBuilder {
    private val mediator = Mediator()
    private var repositoryFactory: RepositoryFactory = InMemoryRepositoryFactory()
    private var currentGame: CurrentGame = CurrentGame()

    fun repositoryFactory(repositoryFactory: RepositoryFactory, cached: Boolean = false): RepositoryFactory {
        this.repositoryFactory = repositoryFactory
        if (cached) { this.repositoryFactory = this.repositoryFactory.cached() }
        return this.repositoryFactory
    }

    fun currentGame() = currentGame

    fun build(): Core {
        return Core(CoreConfiguration(mediator, repositoryFactory, currentGame))
    }
}

fun coreModule(addDetails: CoreBuilder.() -> Unit): Core {
    return CoreBuilder().also(addDetails).build()
}
