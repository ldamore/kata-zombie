package ldamore.zombies.core

import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivor
import ldamore.zombies.core.application.commands.addZombie.AddZombie
import ldamore.zombies.core.application.commands.startGame.StartGame
import ldamore.zombies.core.application.queries.getGame.GetGame
import ldamore.zombies.core.application.queries.getSurvivors.GetSurvivors
import ldamore.zombies.core.domain.game.CurrentGame

class CoreStartUp private constructor(private val config: CoreConfiguration) {
    private val currentGame = config.currentGame
    private val repositories = config.repositoryFactory

    init {
        registerCommands()
        registerQueries()
    }

    private fun registerCommands() = with(config.mediator) {
        register(AddSurvivor(repositories.create()))
        register(AddZombie(repositories.create()))
        register(StartGame(currentGame))
    }

    private fun registerQueries() = with(config.mediator) {
        register(GetGame(currentGame))
        register(GetSurvivors(repositories.create()))
    }

    companion object {
        fun startWith(config: CoreConfiguration) = CoreStartUp(config)
    }
}
