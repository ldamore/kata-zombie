import io.github.cdimascio.dotenv.dotenv
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.21"
    id("com.github.johnrengelman.shadow") version "7.0.0" apply false
}

buildscript {
    dependencies {
        classpath("io.github.cdimascio:java-dotenv:5.2.2")
        classpath("org.postgresql:postgresql:42.2.18")
    }
}

val dotenv = dotenv {
    directory = rootProject.rootDir.absolutePath
    ignoreIfMissing = true
}

allprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm") // Para que todos los modulos compilen kotlin con gradle

    group = "ldamore.zombies"
    version = "1.0-SNAPSHOT"

    project.ext {
        set("DB_DRIVER", dotenv["DB_DRIVER"])
        set("DB_NAME", dotenv["DB_NAME"])
        set("DB_HOST", dotenv["DB_HOST"])
        set("DB_PORT", dotenv["DB_PORT"])
        set("DB_USER", dotenv["DB_USER"])
        set("DB_PASSWORD", dotenv["DB_PASSWORD"])
        set("TEST_DB_DRIVER", dotenv["TEST_DB_DRIVER"])
        set("TEST_DB_NAME", dotenv["TEST_DB_NAME"])
        set("TEST_DB_HOST", dotenv["TEST_DB_HOST"])
        set("TEST_DB_PORT", dotenv["TEST_DB_PORT"])
        set("TEST_DB_USER", dotenv["TEST_DB_USER"])
        set("TEST_DB_PASSWORD", dotenv["TEST_DB_PASSWORD"])
        set("GENERATE_JOOQ", dotenv["TEST_DB_PASSWORD"]?.trim()?.toLowerCase() == "true")
    }

    repositories { mavenCentral() } // Para que todos los modulos busquen sus dependencias externas en el repositorio de maven

    // Para que la tarea de compilar produzca codigo java 16
    tasks.withType<KotlinCompile> { kotlinOptions.jvmTarget = "16" }

    // Para que todos los tests de los modulos corran con Junit

    tasks.withType<Test> {
        useJUnitPlatform { excludeTags("slow") }
    }

    tasks.register<Test>("slowTest") {
        group = "verification"
        useJUnitPlatform { includeTags("slow") }
    }

    tasks.register<Test>("allTests") {
        group = "verification"
        useJUnitPlatform { }
    }


    // Dependencias necesarias en todos los modulos
    dependencies {
        implementation(kotlin("reflect"))
        implementation("io.github.cdimascio:java-dotenv:5.1.2")
        testImplementation("org.junit.jupiter:junit-jupiter:5.7.2")
        testImplementation("org.assertj:assertj-core:3.19.0")
        testImplementation("net.bytebuddy:byte-buddy:1.11.1") // Added for mockk compatibility with JDK16
        testImplementation("io.mockk:mockk:1.12.0")
    }

    // Para que los modulos tengan carpetas src, generated, resources, test y test_resources reconocidos por el IDE y Kotlin
    kotlin {
        sourceSets["main"].apply {
            kotlin.srcDirs("src", "generated")
            resources.srcDirs("resources")
        }
        sourceSets["test"].apply {
            kotlin.srcDir("test")
            resources.srcDir("test_resources")
        }
    }

    java {
        sourceSets["main"].apply {
            java.srcDirs("src", "generated")
            resources.srcDirs("resources")
        }
        sourceSets["test"].apply {
            java.srcDir("test")
            resources.srcDir("test_resources")
        }
    }

}
