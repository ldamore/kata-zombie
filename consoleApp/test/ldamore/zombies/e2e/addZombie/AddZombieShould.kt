package ldamore.zombies.e2e.addZombie

import ldamore.zombies.core.domain.zombie.Zombies
import ldamore.zombies.e2e.BaseE2ETest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class AddZombieShould: BaseE2ETest() {
    @Test
    fun `add-zombie should create a new zombie with the specified name`() {
        When {
            simulateReicibeCommand("add-zombie $zombieName")
        } Then {
            assertOutputContains("Zombie $zombieName was created!")
        }

        with(getZombie(zombieName)!!) {
            assertThat(this.name).isEqualTo(zombieName)
            assertThat(this.wounds).isEqualTo(0)
        }
    }

    @Test
    fun `fail if zombie already exist`() {
        When {
            simulateReicibeCommand("add-zombie $zombieName")
            simulateReicibeCommand("add-zombie $zombieName")
        } Then {
            assertOutputContains("Zombie with name: $zombieName already exists")
        }
    }

    //TODO: sarasa
    @Disabled
    @Test
    fun `fail if game was started`() {
        simulateReicibeCommand("add-zombie lautaro")

        assertThat(io.printedMessages).contains("Game is started, a survivor cant be added")
    }

    private fun getZombie(zombieName: String) = repositories.create<Zombies>().get(zombieName)
    private val zombieName = "paul"
}
