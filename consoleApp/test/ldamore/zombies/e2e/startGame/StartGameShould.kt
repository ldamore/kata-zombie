package ldamore.zombies.e2e.startGame

import ldamore.zombies.core.domain.survivor.Survivor
import ldamore.zombies.core.domain.survivor.SurvivorId
import ldamore.zombies.core.domain.survivor.Survivors
import ldamore.zombies.core.domain.zombie.Zombie
import ldamore.zombies.core.domain.zombie.ZombieId
import ldamore.zombies.core.domain.zombie.Zombies
import ldamore.zombies.e2e.BaseE2ETest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class StartGameShould: BaseE2ETest() {
    @Test
    fun `start the game if has any survivor and zombies`() {
        addSurvivorsAndZombies()

        When {
            simulateReicibeCommand("start-game")
        } Then {
            assertOutputContains("The game was started!!!!")
        }

        assertThat(currentGame.getCurrent()?.isActive).isTrue
    }


    private fun addSurvivorsAndZombies() {
        repositories.create<Survivors>().add(Survivor(SurvivorId(1), "lautaro"))
        repositories.create<Zombies>().add(Zombie(ZombieId(1), "paul"))
    }
}
