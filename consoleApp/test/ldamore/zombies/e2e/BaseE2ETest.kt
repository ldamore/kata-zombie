package ldamore.zombies.e2e

import ldamore.zombies.core.Core
import ldamore.zombies.FakeIO
import ldamore.zombies.consoleApp.config.application
import ldamore.zombies.core.domain.game.CurrentGame
import ldamore.zombies.core.infraestructure.persistance.transactions.JdbcTransactionManager
import ldamore.zombies.core.infraestructure.persistance.transactions.Transaction
import ldamore.zombies.core.infraestructure.repositories.RepositoryFactory
import ldamore.zombies.core.jooqRepositories
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag

@Tag("slow")
abstract class BaseE2ETest {
    private lateinit var currentTransaction: Transaction
    private val transactionManager = BaseE2ETest.transactionManager
    protected val io = BaseE2ETest.io
    private val app = application
    protected val repositories = BaseE2ETest.repositories
    protected val currentGame = BaseE2ETest.currentGame

    fun When(lambda: BaseE2ETest.() -> Unit): BaseE2ETest {
        lambda()
        return this
    }

    infix fun Then(lambda: BaseE2ETest.() -> Unit): BaseE2ETest {
        lambda()
        return this
    }

    protected fun assertOutputContains(output: String) {
        assertThat(io.printedMessages).contains(output)
    }

    protected fun simulateReicibeCommand(command: String) {
        io.addInputToRead(command)
        io.addInputToRead("exit")
        app.start()
    }

    @BeforeEach
    fun setup() {
        currentTransaction = transactionManager.beginTransaction()
    }

    @AfterEach
    fun tearDown() {
        currentTransaction.rollback()
    }

    companion object {
        private lateinit var io: FakeIO
        private lateinit var core: Core
        private lateinit var transactionManager: JdbcTransactionManager
        private lateinit var repositories: RepositoryFactory
        private lateinit var currentGame: CurrentGame

        private val application = application {
            io = io(FakeIO()) as FakeIO

            core = core {
                repositories = jooqRepositories(cached = true) {
                    credentialsFromEnv("TEST_DB")
                    transactionManager = transactionManager()
                }
                currentGame = currentGame()
            }
        }
    }
}

