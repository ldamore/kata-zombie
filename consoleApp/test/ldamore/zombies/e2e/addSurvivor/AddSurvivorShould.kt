package ldamore.zombies.e2e.addSurvivor

import ldamore.zombies.core.domain.survivor.Survivors
import ldamore.zombies.e2e.BaseE2ETest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class AddSurvivorShould: BaseE2ETest() {
    @Test
    fun `add-survivor should create a new survivor with the specified name`() {
        When {
            simulateReicibeCommand("add-survivor $survivorName")
        } Then {
            assertOutputContains("Survivor $survivorName was created!")
        }

        with(getSurvivor(survivorName)!!) {
            assertThat(this.name).isEqualTo(survivorName)
            assertThat(this.wounds).isEqualTo(0)
        }
    }

    @Test
    fun `fail if survivor already exist`() {
        When {
            simulateReicibeCommand("add-survivor $survivorName")
            simulateReicibeCommand("add-survivor $survivorName")
        } Then {
            assertOutputContains("Survivor with name: $survivorName already exists")
        }
    }

    //TODO: sarasa
    @Disabled
    fun `fail if game was started`() {
        simulateReicibeCommand("add-survivor lautaro")

        assertThat(io.printedMessages).contains("Game is started, a survivor cant be added")
    }

    private fun getSurvivor(survivorName: String) = repositories.create<Survivors>().get(survivorName)
    private val survivorName = "lautaro"
}
