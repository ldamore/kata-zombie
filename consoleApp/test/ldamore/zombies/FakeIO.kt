package ldamore.zombies

import ldamore.zombies.consoleApp.console.io.IO

class FakeIO: IO {
    var inputsToRead = mutableListOf<String>()
    val printedMessages = mutableListOf<String>()

    override fun writeLine(message: String) {
        printedMessages.add(message)
    }

    override fun readLine(): String {
        return inputsToRead.removeFirst()
    }

    fun addInputToRead(input: String) {
        inputsToRead.add(input)
    }
}
