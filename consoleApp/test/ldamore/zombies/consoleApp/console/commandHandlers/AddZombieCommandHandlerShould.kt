package ldamore.zombies.consoleApp.console.commandHandlers

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import ldamore.zombies.core.Core
import ldamore.zombies.core.CoreConfiguration
import ldamore.zombies.core.application.Mediator
import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivor
import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivorCommand
import ldamore.zombies.consoleApp.console.commandProcessor.Command
import ldamore.zombies.consoleApp.console.commandProcessor.commandHandlers.AddSurvivorCommandHandler
import ldamore.zombies.consoleApp.console.commandProcessor.commandHandlers.AddZombieCommandHandler
import ldamore.zombies.core.application.commands.addZombie.AddZombie
import ldamore.zombies.core.application.commands.addZombie.AddZombieCommand
import ldamore.zombies.core.domain.game.CurrentGame
import ldamore.zombies.core.domain.survivor.SurvivorNameAlreadyExist
import ldamore.zombies.core.domain.zombie.ZombieNameAlreadyExist
import ldamore.zombies.core.infraestructure.repositories.inMemory.InMemoryRepositoryFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AddZombieCommandHandlerShould {
    @Test
    fun `call the add zombie handler with the given name`() {
        val command = Command("add-zombie", listOf(zombieName))

        handler.handle(command)

        verify { addZombie.execute(AddZombieCommand(zombieName)) }
    }

    @Test
    fun `print successfully message when survivor was created`() {
        val command = Command("add-zombie", listOf(zombieName))

        val response = handler.handle(command)

        assertThat(response).isEqualTo("Zombie $zombieName was created!")
    }

    @Test
    fun `print error message when survivor has already created`() {
        val command = Command("add-zombie", listOf(zombieName))
        every { addZombie.execute(any()) } throws ZombieNameAlreadyExist(zombieName)

        val response = handler.handle(command)

        assertThat(response).isEqualTo("Zombie with name: $zombieName already exists")
    }

    @BeforeEach
    fun setup() {
        mediator.register(addZombie)
    }

    private val zombieName = "paul"
    private val mediator = Mediator()
    private val core = Core(CoreConfiguration(mediator, InMemoryRepositoryFactory().cached(), CurrentGame()))
    private val handler = AddZombieCommandHandler(core)
    private val addZombie = mockk<AddZombie>(relaxed = true)
}