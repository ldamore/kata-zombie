package ldamore.zombies.consoleApp.console.commandHandlers

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import ldamore.zombies.core.Core
import ldamore.zombies.core.CoreConfiguration
import ldamore.zombies.core.application.Mediator
import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivor
import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivorCommand
import ldamore.zombies.consoleApp.console.commandProcessor.Command
import ldamore.zombies.consoleApp.console.commandProcessor.commandHandlers.AddSurvivorCommandHandler
import ldamore.zombies.core.domain.game.CurrentGame
import ldamore.zombies.core.domain.survivor.SurvivorNameAlreadyExist
import ldamore.zombies.core.infraestructure.repositories.inMemory.InMemoryRepositoryFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AddSurvivorCommandHandlerShould {
    @Test
    fun `call the add survivor handler with the given name`() {
        val command = Command("add-survivor", listOf(survivorName))

        handler.handle(command)

        verify { addSurvivor.execute(AddSurvivorCommand(survivorName)) }
    }

    @Test
    fun `print successfully message when survivor was created`() {
        val command = Command("add-survivor", listOf(survivorName))

        val response = handler.handle(command)

        assertThat(response).isEqualTo("Survivor $survivorName was created!")
    }

    @Test
    fun `print error message when survivor has already created`() {
        val command = Command("add-survivor", listOf(survivorName))
        every { addSurvivor.execute(any()) } throws SurvivorNameAlreadyExist(survivorName)

        val response = handler.handle(command)

        assertThat(response).isEqualTo("Survivor with name: $survivorName already exists")
    }

    @BeforeEach
    fun setup() {
        mediator.register(addSurvivor)
    }

    private val survivorName = "lautaro"
    private val mediator = Mediator()
    private val core = Core(CoreConfiguration(mediator, InMemoryRepositoryFactory().cached(), CurrentGame()))
    private val handler = AddSurvivorCommandHandler(core)
    private val addSurvivor = mockk<AddSurvivor>(relaxed = true)
}