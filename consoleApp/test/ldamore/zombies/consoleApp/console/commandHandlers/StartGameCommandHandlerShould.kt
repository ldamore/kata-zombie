package ldamore.zombies.consoleApp.console.commandHandlers

import io.mockk.mockk
import io.mockk.verify
import ldamore.zombies.consoleApp.console.commandProcessor.Command
import ldamore.zombies.core.Core
import ldamore.zombies.core.CoreConfiguration
import ldamore.zombies.core.application.Mediator
import ldamore.zombies.core.application.commands.startGame.StartGame
import ldamore.zombies.core.application.commands.startGame.StartGameCommand
import ldamore.zombies.core.domain.game.CurrentGame
import ldamore.zombies.core.infraestructure.repositories.inMemory.InMemoryRepositoryFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class StartGameCommandHandlerShould {
    @Test
    fun `call start game`() {
        val command = Command("start-game")

        handler.handle(command)

        verify { startGame.execute(StartGameCommand()) }
    }

    @Test
    fun `print successfully message when game is started`() {
        val command = Command("start-game")

        val response = handler.handle(command)

        assertThat(response).isEqualTo("The game was started!!!!")
    }

    @BeforeEach
    fun setup() {
        mediator.register(startGame)
    }

    private val mediator = Mediator()
    private val currentGame = CurrentGame()
    private val core = Core(CoreConfiguration(mediator, InMemoryRepositoryFactory().cached(), currentGame))
    private val handler = StartGameCommandHandler(core)
    private val startGame = mockk<StartGame>(relaxed = true)
}