package ldamore.zombies.consoleApp.console

import ldamore.zombies.FakeIO
import ldamore.zombies.consoleApp.console.commandProcessor.Command
import ldamore.zombies.consoleApp.console.commandProcessor.CommandHandler
import ldamore.zombies.consoleApp.console.commandProcessor.CommandHandlerFactory
import ldamore.zombies.consoleApp.console.commandProcessor.CommandProcessor
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CommandProcessorShould {
    @Test
    fun `process the specified command`() {
        io.willReadAndExit("some-command")

        commandProcessor().run()

        assertThat(io.printedMessages).contains("some output response")
    }

    @Test
    fun `outputs a unknown command error when command is invalid`() {
        io.willReadAndExit("sarasa")

        commandProcessor().run()

        assertThat(io.printedMessages).contains("ERROR: invalid command sarasa")
    }

    @Test
    fun `execute multiple commands`() {
        io.willReadAndExit("some-command", "some-command")

        commandProcessor().run()

        assertThat(io.printedMessages).contains("some output response", "some output response")
    }

    @Test
    fun `exit command closes app`() {
        io.addInputToRead("exit")

        commandProcessor().run()

        assertThat(io.printedMessages).containsExactly("Shutting down...")
    }

    private fun FakeIO.willReadAndExit(vararg inputs: String) {
        inputs.forEach { addInputToRead(it) }.also { this.addInputToRead("exit") }
    }

    private fun commandProcessor(): CommandProcessor {
        val commandHandlerFactory = CommandHandlerFactory().apply { register(SomeCommandHandler()) }
        return CommandProcessor(io, commandHandlerFactory)
    }

    private val io = FakeIO()
    private class SomeCommandHandler: CommandHandler {
        override val commandName = "some-command"
        override fun handle(command: Command) = "some output response"
    }
}