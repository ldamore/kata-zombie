package ldamore.zombies.consoleApp.config

import ldamore.zombies.consoleApp.Application
import ldamore.zombies.core.Core
import ldamore.zombies.core.CoreBuilder
import ldamore.zombies.consoleApp.console.io.IO
import ldamore.zombies.consoleApp.console.io.SystemIO

class ApplicationBuilder {
    private var io: IO = SystemIO()
    private var core: Core? = null

    fun core(addDetails: CoreBuilder.() -> Unit): Core {
        core = CoreBuilder().also(addDetails).build()
        return core!!
    }

    fun io(io: IO): IO {
        this.io = io
        return io
    }

    fun build(): Application {
        if (core == null) throw UninitializedPropertyAccessException("You must initialize core")
        return Application(ApplicationConfiguration(io, core!!))
    }
}

fun application(addDetails: ApplicationBuilder.() -> Unit): Application {
    return ApplicationBuilder().also(addDetails).build()
}
