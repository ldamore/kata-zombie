package ldamore.zombies.consoleApp.config

import ldamore.zombies.core.Core
import ldamore.zombies.consoleApp.console.io.IO

class ApplicationConfiguration(
    val IO: IO,
    val core: Core,
)
