package ldamore.zombies.consoleApp

import ldamore.zombies.consoleApp.config.ApplicationConfiguration
import ldamore.zombies.consoleApp.console.commandHandlers.StartGameCommandHandler
import ldamore.zombies.consoleApp.console.commandProcessor.CommandHandlerFactory
import ldamore.zombies.consoleApp.console.commandProcessor.CommandProcessor
import ldamore.zombies.consoleApp.console.commandProcessor.commandHandlers.AddSurvivorCommandHandler
import ldamore.zombies.consoleApp.console.commandProcessor.commandHandlers.AddZombieCommandHandler

class Application(private val config: ApplicationConfiguration) {
    private val io = config.IO
    private val handlerFactory = CommandHandlerFactory()
    private val commandProcessor = CommandProcessor(io, handlerFactory)

    init {
        initializeCommands()
    }

    fun start() {
        io.writeLine("Welcome to the survivors game!")
        commandProcessor.run()
    }

    private fun initializeCommands() {
        handlerFactory.apply {
            register(AddSurvivorCommandHandler(config.core))
            register(AddZombieCommandHandler(config.core))
            register(StartGameCommandHandler(config.core))
        }
    }
}
