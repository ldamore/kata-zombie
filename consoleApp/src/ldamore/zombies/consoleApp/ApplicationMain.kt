package ldamore.zombies

import ldamore.zombies.consoleApp.config.application
import ldamore.zombies.core.jooqRepositories

fun main() {
    val app = application {
        core {
            jooqRepositories {
                credentialsFromEnv()
            }
        }
    }
    app.start()
}
