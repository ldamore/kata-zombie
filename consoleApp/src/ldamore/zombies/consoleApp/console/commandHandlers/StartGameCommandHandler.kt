package ldamore.zombies.consoleApp.console.commandHandlers

import ldamore.zombies.consoleApp.console.commandProcessor.Command
import ldamore.zombies.consoleApp.console.commandProcessor.CommandHandler
import ldamore.zombies.core.Core
import ldamore.zombies.core.application.commands.startGame.StartGameCommand

class StartGameCommandHandler(private val core: Core): CommandHandler {
    override val commandName = "start-game"

    override fun handle(command: Command): String {
        core.command(StartGameCommand())
        return "The game was started!!!!"
    }
}
