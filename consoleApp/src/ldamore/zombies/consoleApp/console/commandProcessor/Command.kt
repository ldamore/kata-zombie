package ldamore.zombies.consoleApp.console.commandProcessor

class Command(val name: String, val parameters: List<String> = listOf())
