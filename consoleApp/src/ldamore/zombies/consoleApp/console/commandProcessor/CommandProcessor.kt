package ldamore.zombies.consoleApp.console.commandProcessor

import ldamore.zombies.consoleApp.console.io.IO

class CommandProcessor(private val io: IO, private val commandHandlerFactory: CommandHandlerFactory) {
    fun run() {
        while (true) {
            val command = readCommand()
            if (isExit(command)) break
            process(command)
        }
        io.writeLine("Shutting down...")
    }

    private fun isExit(command: Command) = command.name == "exit"

    private fun process(command: Command) {
        try {
            val outputStr = commandHandlerFactory.create(command.name).handle(command)
            if (outputStr != null) io.writeLine(outputStr)
        } catch (e: UnknownCommandError) {
            io.writeLine("ERROR: invalid command ${command.name}")
        }
    }

    private fun readCommand(): Command {
        val line = io.readLine()
        val split = line.split(" ")
        return Command(split.first(), split.drop(1))
    }
}