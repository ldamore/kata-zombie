package ldamore.zombies.consoleApp.console.commandProcessor.commandHandlers

import ldamore.zombies.core.Core
import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivorCommand
import ldamore.zombies.consoleApp.console.commandProcessor.Command
import ldamore.zombies.consoleApp.console.commandProcessor.CommandHandler
import ldamore.zombies.core.domain.survivor.SurvivorNameAlreadyExist

class AddSurvivorCommandHandler(private val core: Core): CommandHandler {
    override val commandName = "add-survivor"

    override fun handle(command: Command): String {
        val survivorName = command.parameters.first()
        return addSurvivor(survivorName)
    }

    private fun addSurvivor(survivorName: String): String {
        try {
            core.command(AddSurvivorCommand(survivorName))
            return "Survivor $survivorName was created!"
        } catch (e: SurvivorNameAlreadyExist) {
            return "Survivor with name: $survivorName already exists"
        }
    }
}
