package ldamore.zombies.consoleApp.console.commandProcessor.commandHandlers

import ldamore.zombies.core.Core
import ldamore.zombies.core.application.commands.addSurvivor.AddSurvivorCommand
import ldamore.zombies.consoleApp.console.commandProcessor.Command
import ldamore.zombies.consoleApp.console.commandProcessor.CommandHandler
import ldamore.zombies.core.application.commands.addZombie.AddZombieCommand
import ldamore.zombies.core.domain.survivor.SurvivorNameAlreadyExist
import ldamore.zombies.core.domain.zombie.ZombieNameAlreadyExist

class AddZombieCommandHandler(private val core: Core): CommandHandler {
    override val commandName = "add-zombie"

    override fun handle(command: Command): String {
        val zombieName = command.parameters.first()
        return addZombie(zombieName)
    }

    private fun addZombie(zombieName: String): String {
        try {
            core.command(AddZombieCommand(zombieName))
            return "Zombie $zombieName was created!"
        } catch (e: ZombieNameAlreadyExist) {
            return "Zombie with name: $zombieName already exists"
        }
    }
}
