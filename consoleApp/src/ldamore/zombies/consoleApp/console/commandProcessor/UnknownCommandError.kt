package ldamore.zombies.consoleApp.console.commandProcessor

import ldamore.zombies.core.domain.DomainError

class UnknownCommandError(command: String): DomainError("Unknown command: $command")
