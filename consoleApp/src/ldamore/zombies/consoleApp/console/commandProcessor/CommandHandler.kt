package ldamore.zombies.consoleApp.console.commandProcessor



interface CommandHandler {
    val commandName: String

    fun handle(command: Command): String
}