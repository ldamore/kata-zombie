package ldamore.zombies.consoleApp.console.io

interface IO {
    fun writeLine(message: String)
    fun readLine(): String
}