package ldamore.zombies.consoleApp.console.io

class SystemIO: IO {
    override fun writeLine(message: String) {
        println(message)
    }

    override fun readLine(): String {
        return kotlin.io.readLine()!!
    }
}