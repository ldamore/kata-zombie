import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    id("com.github.johnrengelman.shadow")
}

dependencies {
    implementation(project(":core"))
    testImplementation(project(":core").dependencyProject.sourceSets["test"].output)
}

tasks.getByName<ShadowJar>("shadowJar") {
    archiveFileName.set("zombies_console.jar")
    manifest {
        attributes(mapOf(
            "Main-Class" to "ldamore.zombies.ApplicationMainKt",
            "VERSION" to project.version
        ))
    }
}