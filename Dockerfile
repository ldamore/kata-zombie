FROM gradle:7.0.2-jdk16 as builder

USER root
ARG COMMIT_SHA

COPY . .

RUN printf %s ".${COMMIT_SHA}" >> VERSION

RUN gradle --parallel --build-cache -Dorg.gradle.console=plain -Dorg.gradle.daemon=false shadowJar

###########################################################################
FROM openjdk:16-alpine

WORKDIR /app

COPY --from=builder /home/gradle/consoleApp/build/libs/zombies_console.jar zombies_console.jar

CMD java -Xms256m -Xmx256m -Xss512k -jar zombies_console.jar
